var TelegramBot = require('node-telegram-bot-api');
var request = require('request');
var pinyin = require('pinyin');

var token = '202213649:AAEVV-eY7VisGWnGjFJmf-9O8otEH90RYuo';

var bot = new TelegramBot(token, {polling: true});
var BASE_URL = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20160301T032928Z.ff1cf86eba8e5bc2.450c7eac001c09018a01e72b8f75725857bd295d";

function pinyinFromChinese(chineseText){
    var pinyinArray = pinyin(chineseText);
    var pinyinString = "";
    for(var i = 0; i < pinyinArray.length; i++){
        pinyinString += pinyinArray[i] + " ";
    }
    return pinyinString;
}

function toEnglish(msg, string){
    var url = BASE_URL + "&text=" + encodeURI(string);
    url += "&lang=zh-en";

    request.post({ url:url }, function(err, httpResponse, body){
        var object = JSON.parse(body);
        console.log(body);
        if(object.code !== 200){
            var errorString = "There was an error in translation:\n";
            errorString += object.message + "\n\n";
            errorString += "Please contact @Lignite if you believe this was not your fault!";

            console.log("Messaging " + errorString);
            bot.sendMessage(msg.chat.id, errorString, {reply_to_message_id: msg.message_id});
        }
        else{
            var text = object.text;
            var fixedString = "";
            for(var i = 0; i < text.length; i++){
                fixedString += text[i] + "\n";
            }
            console.log("Messaging " + fixedString);
            bot.sendMessage(msg.chat.id, fixedString, {reply_to_message_id: msg.message_id});
        }
    });
}

function toChinese(msg, string){
    var url = BASE_URL + "&text=" + encodeURI(string);
    url += "&lang=en-zh";

    request.post({ url:url }, function(err, httpResponse, body){
        var object = JSON.parse(body);
        console.log(body);
        if(object.code !== 200){
            var errorString = "There was an error in translation:\n";
            errorString += object.message + "\n\n";
            errorString += "Please contact @Lignite if you believe this was not your fault!";

            console.log("Messaging " + errorString);
            bot.sendMessage(msg.chat.id, errorString, {reply_to_message_id: msg.message_id});
        }
        else{
            var text = object.text;
            var fixedString = "";
            for(var i = 0; i < text.length; i++){
                fixedString += text[i] + "\n";
            }
            var pinyinArray = pinyin(fixedString);
            var pinyinString = "";
            console.log("Got an array " + pinyinArray);
            for(var i = 0; i < pinyinArray.length; i++){
                pinyinString += pinyinArray[i] + " ";
            }
            fixedString += pinyinString;
            console.log("Messaging " + fixedString);
            bot.sendMessage(msg.chat.id, fixedString, {reply_to_message_id: msg.message_id});
        }
    });
}

function toPinyin(msg, string){
    var pinyinString = pinyinFromChinese(string);
    console.log("messaging " + pinyinString);
    bot.sendMessage(msg.chat.id, pinyinString, {reply_to_message_id: msg.message_id});
}

bot.onText(/\/toenglish/, function (msg, match) {
    if(msg.text !== "/toenglish" && msg.text !== "/toenglish@ChineseTranslationBot"){
        return;
    }
    if(msg.reply_to_message){
        toEnglish(msg, msg.reply_to_message.text);
    }
    else{
        bot.sendMessage(msg.chat.id, "Enter in text after the command. For example, '/toenglish 你好'. Or, reply to a Chinese message with /toenglish.", {reply_to_message_id: msg.message_id});
    }
});

bot.onText(/\/tochinese/, function (msg, match) {
    if(msg.text !== "/tochinese" && msg.text !== "/tochinese@ChineseTranslationBot"){
        return;
    }
    if(msg.reply_to_message){
        toChinese(msg, msg.reply_to_message.text);
    }
    else{
        bot.sendMessage(msg.chat.id, "Enter in text after the command. For example, '/tochinese You good?'. Or, reply to an English message with /tochinese.", {reply_to_message_id: msg.message_id});
    }
});

bot.onText(/\/topinyin/, function (msg, match) {
    if(msg.text !== "/topinyin" && msg.text !== "/topinyin@ChineseTranslationBot"){
        return;
    }
    if(msg.reply_to_message){
        toPinyin(msg, msg.reply_to_message.text);
    }
    else{
        bot.sendMessage(msg.chat.id, "Enter in text after the command. For example, '/topinyin 你好'. Or, reply to a Chinese message with /topinyin.", {reply_to_message_id: msg.message_id});
    }
});

bot.onText(/\/toenglish (.+)/, function (msg, match) {
    toEnglish(msg, match[1]);
});

bot.onText(/\/tochinese (.+)/, function (msg, match) {
    toChinese(msg, match[1]);
});

bot.onText(/\/topinyin (.+)/, function (msg, match) {
    toPinyin(msg, match[1]);
});

bot.onText(/\/source/, function (msg, match) {
    bot.sendMessage(msg.chat.id, "Developed by @Lignite!\n\nHere's my source code: https://gitlab.com/edwinfinch/chinese_translation_bot");
});